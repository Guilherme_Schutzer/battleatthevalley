﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TreeController : Damageable {

    private Color color;
    private Color colorStep;
    private float colorChangeRate;

    private void Start()
    {
        color = Renderers[0].material.color;
        CurrentHealth = MaxHealth;
    }

    public override void Blink(float totalSeconds)
    {
        Renderers[0].material.color -= (color - Color.red * 0.1f) * colorChangeRate;
        isVulnerable = true;
    }

    public override void TakeDamage(float damage)
    {
        if (CurrentHealth <= 0)
            return;

        colorChangeRate = damage / MaxHealth;
        base.TakeDamage(damage);
    }

    public override void Death()
    {
        Destroy(gameObject);
    }

    public override void TimedLock(float seconds) { }
}
