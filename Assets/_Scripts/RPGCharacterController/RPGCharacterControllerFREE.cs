﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using _Scripts;

namespace RPGCharacterAnims{

	[RequireComponent(typeof(RPGCharacterMovementController))]
	[RequireComponent(typeof(RPGCharacterInputController))]
	public class RPGCharacterControllerFREE : Damageable
    {

		//Components.
		[HideInInspector]	public RPGCharacterMovementController RpgCharacterMovementController;
		[HideInInspector]	public RPGCharacterInputController RpgCharacterInputController;
		public Camera MainCamera;
	    public GameObject Staff;

		//Strafing/action.
		[HideInInspector] public bool IsDead = false;
		[HideInInspector] public bool CanAction = true;
		[HideInInspector] public bool IsStrafing = false;
	    [HideInInspector] public string PlayerName;
	    
	    private GameInfo info;
	    private GameObject homeScreen;
	    private GameObject overlayMessage;
	    private string winnerName = null;
	    private string serverAddress;

		#region Initialization
	    
		void Awake(){
			RpgCharacterMovementController = GetComponent<RPGCharacterMovementController>();
			RpgCharacterInputController = GetComponent<RPGCharacterInputController>();
			homeScreen = GameObject.FindWithTag("HomeScreen");
			overlayMessage = GameObject.FindWithTag("OverlayMessage");
			
			MainCamera = Camera.main;

			info = GameObject.FindGameObjectWithTag("Info").GetComponent<GameInfo>();
			
			if(animator == null){
				Debug.LogError("ERROR: There is no animator for character.");
				Destroy(this);
			}
			if(MainCamera == null){
				Debug.LogError("ERROR: There is no target for character.");
				Destroy(this);
			}
		}

	    new void Start()
	    {
		    base.Start();
	    }

	    public override void OnStartLocalPlayer()
	    {
		    var cameraController = MainCamera.GetComponent<CameraController>();	
		    cameraController.Target = gameObject;
		    var name = homeScreen.transform.GetChild(0).GetComponent<InputField>().text;
		    serverAddress = homeScreen.transform.GetChild(0).GetComponent<InputField>().text;
		    CmdRegisterPlayer(name);
		    homeScreen.SetActive(false);
	    }

	    [Command]
	    void CmdRegisterPlayer(string name)
	    {
		    RpcRegisterPlayer(name);
		    info.NumberOfPlayers += 1;
	    }

	    [ClientRpc]
	    void RpcRegisterPlayer(string name)
	    {
		    PlayerName = name;
		    var text = gameObject.GetComponentInChildren<Text>();
		    text.text = name;
		    text.color = Color.white;
		    info.Players.Add(gameObject);
	    }

        void OnDestroy()
        {
	        if (!isLocalPlayer)
		        return;
	        
            var cameraController = MainCamera.GetComponent<CameraController>();
            cameraController.Target = null;
        }

		#endregion

		#region Updates

		void Update(){
			if(RpgCharacterMovementController.MaintainingGround()){
				//Revive.
				if(RpgCharacterInputController.inputDeath){
					if(IsDead){
						Revive();
					}
				}
				if(CanAction){
					Strafing();
					Rolling();
					//Hit.
					if(RpgCharacterInputController.inputLightHit){
						GetHit(new Vector3(0,0,0), 0);
					}
					//Death.
					if(RpgCharacterInputController.inputDeath){
						if(!IsDead){
							Death();
						}
						else{
							Revive();
						}
					}
					//Attacks.
					if(RpgCharacterInputController.inputAttackL){
						Attack(1);
					}
					if(RpgCharacterInputController.inputAttackR){
						Attack(2);
					}
					if(RpgCharacterInputController.inputLightHit){
						GetHit(new Vector3(0,0,0), 0);
					}
					//Shooting / Navmesh.
					if(Input.GetMouseButtonDown(0)){
						if(RpgCharacterMovementController.useMeshNav){
							RaycastHit hit;
							if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100)){
								RpgCharacterMovementController.navMeshAgent.destination = hit.point;
							}
						}
					}
				}
			}
			//Slow time toggle.
			if(Input.GetKeyDown(KeyCode.T)){
				if(Time.timeScale != 1){
					Time.timeScale = 1;
				}
				else{
					Time.timeScale = 0.05f;
				}
			}
			//Pause toggle.
			if(Input.GetKeyDown(KeyCode.P)){
				if(Time.timeScale != 1){
					Time.timeScale = 1;
				}
				else{
					Time.timeScale = 0f;
				}
			}
		}

		#endregion

		#region Turning

		//Turning.
		public IEnumerator _Turning(int direction){
			if(direction == 1){
				Lock(true, true, true, 0, 0.55f);
				animator.SetTrigger("TurnLeftTrigger");
			}
			if(direction == 2){
				Lock(true, true, true, 0, 0.55f);
				animator.SetTrigger("TurnRightTrigger");
			}
			yield return null;
		}

		#endregion

		#region Combat

		/// <summary>
		/// Dodge the specified direction.
		/// </summary>
		/// <param name="direction">1 = Left, 2 = Right</param>		
		public IEnumerator _Dodge(int direction){
			animator.SetInteger("Action", direction);
			animator.SetTrigger("DodgeTrigger");
			Lock(true, true, true, 0, 0.55f);
			yield return null;
		}

		//0 = No side
		//1 = Left
		//2 = Right
		public void Attack(int attackSide){
			int attackNumber = 0;
			if(CanAction){
				//Ground attacks.
				if(RpgCharacterMovementController.MaintainingGround()){
					//Stationary attack.
					//Armed or Unarmed.
					int maxAttacks = 3;
					//Left attacks.
					if(attackSide == 1){
						animator.SetInteger("AttackSide", 1);
                        attackNumber = 1;
						gameObject.GetComponent<Magic>().CmdFireMissile();
					}
					//Right attacks.
					else if(attackSide == 2){
						animator.SetInteger("AttackSide", 2);
						attackNumber = Random.Range(4, maxAttacks + 4);
					}
					//Set the Locks.
					Lock(true, true, true, 0, 0.7f);
				}
                else
                {
                    if(attackSide == 1)
                    {
                        animator.SetInteger("AttackSide", 1);
                        attackNumber = 1;
                        gameObject.GetComponent<Magic>().CmdFireMissile();
                    }
                    Lock(true, true, true, 0, 0.7f);
                }
				//Trigger the animation.
				animator.SetInteger("Action", attackNumber);
				animator.SetTrigger("AttackTrigger");
			}
		}

		public void AttackKick(int kickSide){
			if(RpgCharacterMovementController.MaintainingGround()){
				animator.SetInteger("Action", kickSide);
				animator.SetTrigger("AttackKickTrigger");
				Lock(true, true, true, 0, 0.9f);
			}
		}

		void Strafing(){
			if(RpgCharacterInputController.inputStrafe || RpgCharacterInputController.inputTargetBlock > 0.8f){
				animator.SetBool("Strafing", true);
				IsStrafing = true;
			}
			else{
				IsStrafing = false;
				animator.SetBool("Strafing", false);
			}
		}

		void Rolling(){
			if(!RpgCharacterMovementController.isRolling){
				if(RpgCharacterInputController.inputRoll){
					RpgCharacterMovementController.DirectionalRoll();
				}
			}
		}

		public override void GetHit(Vector3 direction, float damage){

            if (!isVulnerable)
                return;

            isVulnerable = false;
            TakeDamage(damage);
			animator.SetInteger("Action", 1);
			animator.SetTrigger("GetHitTrigger");
			
            //Apply directional knockback force.
            StartCoroutine(RpgCharacterMovementController._Knockback(direction, 8*((int)damage/10), 4));
		}
	

        public override void Death()
        {
	        if (isLocalPlayer)
	        {
		        IsDead = true;
		        DisplayDeathMessage();	
		        CmdDeath(PlayerName);
	        } 
        }

	    void DisplayDeathMessage()
	    {
		    overlayMessage.GetComponentInChildren<Text>().text = "YOU DIED";
		    overlayMessage.GetComponentInChildren<Text>().color = Color.red;
	    }
	    
	    public override void Blink(float totalSeconds)
	    {
		    CmdBlink(totalSeconds);
	    }
	    
	    [Command]
	    private void CmdBlink(float totalSeconds)
	    {
			RpcBlink(totalSeconds);
	    }

	    [ClientRpc]
	    public virtual void RpcBlink(float totalSeconds)
	    {
		    const float fixedInterval = 0.1f;
		    var numBlink = totalSeconds / fixedInterval / 2;
		    StartCoroutine(_Blink((int)numBlink, fixedInterval));
	    }

	    [Command]
	    void CmdDeath(string name)
	    {
		    RpcDeath();		    
		    info.RegisterDeath(name);
	    }

	    [ClientRpc]
	    void RpcDeath()
	    {
		    animator.SetTrigger("Death1Trigger");
		    networkAnimator.SetTrigger("Death1Trigger");
		    gameObject.GetComponent<CapsuleCollider>().enabled = false;
		    Lock(true, true, false, 0f, 0f);
		    IsDead = true;
	    }

	    [ClientRpc]
	    public void RpcFinishGame(string saveJson)
	    {
		    if (!isLocalPlayer)
			    return;

		    if (!isServer)
		    {
				SaveManager.Instance.SaveGame(saveJson);
				ProfileManager.Instance.RefreshEntries();
			    
		    }		  
		    StartCoroutine(EndRoutines());
	    }
	    
	    IEnumerator EndRoutines()
	    {
		    CmdGetWinnerName();
		    yield return new WaitUntil(() => winnerName != null);
		    
		    var text = overlayMessage.GetComponentInChildren<Text>();
		    if (IsDead)
		    {
			    text.text = "Game Over!\n" + winnerName + " has won!";
			    text.color = Color.red;
		    }
		    else
		    {
			    text.text = "Game Over!\n You won!";
			    text.color = Color.blue;
		    }

		    text.text = "";
		    homeScreen.SetActive(true);
		    GameObject.FindWithTag("ServerName").GetComponent<Text>().text = "Server: " + serverAddress;
		    
		    if (!isServer)
				GameObject.FindWithTag("NetworkManager").GetComponent<NetworkManager>().StopClient();
			
	    }
	    
	    [Command]
	    void CmdGetWinnerName()
	    {
		    RpcGetWinnerName(info.WinnerName);
	    }

		[ClientRpc]
	    void RpcGetWinnerName(string winnerName)
		{
			this.winnerName = winnerName;
		}

		public void Revive(){
			animator.SetTrigger("Revive1Trigger");
			Lock(true, true, true, 0f, 1f);
			IsDead = false;
		}

		#endregion

		#region LockUnlock

		/// <summary>
		/// Keep character from doing actions.
		/// </summary>
		void LockAction(){
			CanAction = false;
		}

		/// <summary>
		/// Let character move and act again.
		/// </summary>
		void UnLock(bool movement, bool actions){
			if(movement){
				RpgCharacterMovementController.UnlockMovement();
			}
			if(actions){
				CanAction = true;
			}
		}

		#endregion

		#region Misc

		//Placeholder functions for Animation events.
		public void Hit(){
		}

		public void Shoot(){
		}

		public void FootR(){
		}

		public void FootL(){
		}

		public void Jump(){
		}

		public void Land(){
		}

		IEnumerator _GetCurrentAnimationLength(){
			yield return new WaitForEndOfFrame();
			float f = (float)animator.GetCurrentAnimatorClipInfo(0).Length;
			Debug.Log(f);
		}

		/// <summary>
		/// Lock character movement and/or action, on a delay for a set time.
		/// </summary>
		/// <param name="lockMovement">If set to <c>true</c> lock movement.</param>
		/// <param name="lockAction">If set to <c>true</c> lock action.</param>
		/// <param name="timed">If set to <c>true</c> timed.</param>
		/// <param name="delayTime">Delay time.</param>
		/// <param name="lockTime">Lock time.</param>
		public void Lock(bool lockMovement, bool lockAction, bool timed, float delayTime, float lockTime){
			StopCoroutine("_Lock");
			StartCoroutine(_Lock(lockMovement, lockAction, timed, delayTime, lockTime));
		}

        public override void TimedLock(float seconds)
        {
            Lock(true, true, true, 0.1f, seconds);
        }

		//Timed -1 = infinite, 0 = no, 1 = yes.
		public IEnumerator _Lock(bool lockMovement, bool lockAction, bool timed, float delayTime, float lockTime){
			if(delayTime > 0){
				yield return new WaitForSeconds(delayTime);
			}
			if(lockMovement){
				RpgCharacterMovementController.LockMovement();
			}
			if(lockAction){
				LockAction();
			}
			if(timed){
				if(lockTime > 0){
					yield return new WaitForSeconds(lockTime);
				}
				UnLock(lockMovement, lockAction);
			}
		}

		/// <summary>
		/// Sets the animator state.
		/// </summary>
		/// <param name="weapon">Weapon.</param>
		/// <param name="weaponSwitch">Weapon switch.</param>
		/// <param name="Lweapon">Lweapon.</param>
		/// <param name="Rweapon">Rweapon.</param>
		/// <param name="weaponSide">Weapon side.</param>
		void SetAnimator(int weapon, int weaponSwitch, int Lweapon, int Rweapon, int weaponSide){
			Debug.Log("SETANIMATOR: Weapon:" + weapon + " Weaponswitch:" + weaponSwitch + " Lweapon:" + Lweapon + " Rweapon:" + Rweapon + " Weaponside:" + weaponSide);
			//Set Weapon if applicable.
			if(weapon != -2){
				animator.SetInteger("Weapon", weapon);
			}
			//Set WeaponSwitch if applicable.
			if(weaponSwitch != -2){
				animator.SetInteger("WeaponSwitch", weaponSwitch);
			}
		}

		public void AnimatorDebug(){
			Debug.Log("ANIMATOR SETTINGS---------------------------");
			Debug.Log("Moving: " + animator.GetBool("Moving"));
			Debug.Log("Strafing: " + animator.GetBool("Strafing"));
			Debug.Log("Aiming: " + animator.GetBool("Aiming"));
			Debug.Log("Stunned: " + animator.GetBool("Stunned"));
			Debug.Log("Swimming: " + animator.GetBool("Swimming"));
			Debug.Log("Blocking: " + animator.GetBool("Blocking"));
			Debug.Log("Injured: " + animator.GetBool("Injured"));
			Debug.Log("LeftRight: " + animator.GetInteger("LeftRight"));
			Debug.Log("AttackSide: " + animator.GetInteger("AttackSide"));
			Debug.Log("Jumping: " + animator.GetInteger("Jumping"));
			Debug.Log("Action: " + animator.GetInteger("Action"));
			Debug.Log("Talking: " + animator.GetInteger("Talking"));
			Debug.Log("Velocity X: " + animator.GetFloat("Velocity X"));
			Debug.Log("Velocity Z: " + animator.GetFloat("Velocity Z"));
			Debug.Log("Charge: " + animator.GetFloat("Charge"));
		}

        #endregion

    }
}