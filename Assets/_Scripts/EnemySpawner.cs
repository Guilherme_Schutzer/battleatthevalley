﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class EnemySpawner : NetworkBehaviour
{
	public int NumberOfEnemies;
	
	public override void OnStartServer()
	{
		var childrenScripts = gameObject.transform.GetComponentsInChildren<CharacterSpawner>().ToList();
		for (var i = 0; i < NumberOfEnemies; i++)
		{
			var index = Random.Range(0, childrenScripts.Count);
			var spawner = childrenScripts[index];
			spawner.SpawnCharacter();
			childrenScripts.Remove(spawner);
		}
	}
}
