﻿using System.Collections;
using System.Collections.Generic;
using RPGCharacterAnims;
using UnityEngine;

public class SpeedPowerUp : MonoBehaviour
{
	public float SpeedBonus;
	
	private void OnTriggerEnter(Collider other)
	{
		if (!other.gameObject.CompareTag("Player")) 
			return;

		var player = other.GetComponent<RPGCharacterMovementController>();
		player.runSpeed += SpeedBonus;
		player.walkSpeed += SpeedBonus / 2;
		
		Destroy(gameObject);
	}
}

