﻿using UnityEngine;

public class HealthPowerUp : MonoBehaviour
{

	public float HealthBonus;
	
	private void OnTriggerEnter(Collider other)
	{
		if (!other.gameObject.CompareTag("Player")) 
			return;
		
		var player = other.gameObject.GetComponent<Damageable>();
		player.CurrentHealth += HealthBonus;
		
		if (player.CurrentHealth > player.MaxHealth)
			player.CurrentHealth = player.MaxHealth;
		
		Destroy(gameObject);
	}
}

