﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPowerUp : MonoBehaviour
{

	public float PowerBonus;
	public float SpeedBonus;
	public float SizeBonus;
	
	private void OnTriggerEnter(Collider other)
	{
		if (!other.gameObject.CompareTag("Player")) 
			return;
		
		var magic = other.gameObject.GetComponent<Magic>();
		magic.MagicPower += PowerBonus;
		magic.MagicSpeed += SpeedBonus;
		magic.MagicSize += SizeBonus;
		
		Destroy(gameObject);
	}
}
