﻿using UnityEngine;


public class Missile : MonoBehaviour
{
	public float MagicPower;
	private int count = 0;
	private bool locked;

	private void OnTriggerEnter(Collider other)
	{
		if (locked)
			return;

		locked = true;
		
		if (other.gameObject.CompareTag("Damageable") || other.gameObject.CompareTag("Player"))
		{
			var damageable = other.gameObject.GetComponent<Damageable>();
			if (damageable != null)
			{
				damageable.GetHit(other.gameObject.transform.position - gameObject.transform.position, MagicPower);
			}
		}
		
		Destroy(gameObject);
	}
}

