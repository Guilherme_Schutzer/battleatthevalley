using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class Magic : NetworkBehaviour
{
    public GameObject MagicSpawner;
    public GameObject MissilePrefab;
    public float MagicSpeed;
    public float MagicPower;
    public float MagicSize;

    [Command]
    public void CmdFireMissile()
    {
        if (isServer)
        {
            RpcFireMissile();
        }
    }

    [ClientRpc]
    public void RpcFireMissile()
    {
        StartCoroutine(_WaitAndFire());
    }

    IEnumerator _WaitAndFire()
    {
        yield return new WaitForSeconds(0.4f);
        
        var missile = Instantiate(
            MissilePrefab,
            MagicSpawner.transform.position,
            MagicSpawner.transform.rotation);

        missile.transform.localScale = new Vector3(MagicSize, MagicSize, MagicSize);
        missile.GetComponent<Missile>().MagicPower = MagicPower;
        missile.GetComponent<Rigidbody>().velocity = gameObject.transform.forward * MagicSpeed;
        
        Destroy(missile, 5.0f);
    }        
    
}