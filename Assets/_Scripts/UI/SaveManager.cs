﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
	public SaveInfo Save;

	void Awake()
	{
		_instance = FindObjectOfType<SaveManager>();
		LoadGame();
	}
	private static SaveManager _instance;
	
	public static SaveManager Instance
	{
		get { return _instance ?? (_instance = FindObjectOfType<SaveManager>()); }
	}

	public void SaveGame()
	{
		var savedGame = JsonUtility.ToJson(Save);
		PlayerPrefs.SetString("Save", savedGame);
	}

	public void SaveGame(string savedGame)
	{
		PlayerPrefs.SetString("Save", savedGame);
	}

	public void DeleteSaveGame()
	{
		Save = new SaveInfo();
		SaveGame();
	}

	public void LoadGame()
	{
		var loadedGame = PlayerPrefs.GetString("Save", string.Empty);
		Save = JsonUtility.FromJson<SaveInfo>(loadedGame);
		if (Save == null)
		{
			Save = new SaveInfo();
		}
	}
	
	private void OnDestroy()
	{
		if (_instance == this)
		{
			_instance = null;
		}
	}
}
