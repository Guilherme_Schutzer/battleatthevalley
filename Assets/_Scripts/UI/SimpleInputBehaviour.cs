﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SimpleInputBehaviour : MonoBehaviour
{
    public Selectable TabSelectable;

    private SimpleNetworkHUD hud;
    private InputField field;    

    private void Awake()
    {
        hud = FindObjectOfType<SimpleNetworkHUD>();
        field = GetComponent<InputField>();
    }

    public void SetInteractableName()
    {
        hud.SetInteractableName(field.text != "");
    }

    public void SetInteractableAddress()
    {
        hud.SetInteractableAddress(field.text != "");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab) && EventSystem.current.currentSelectedGameObject == gameObject)
        {
            TabSelectable.Select();
        }
    }
}