﻿using System;
using System.Collections.Generic;

[Serializable]
public class SaveInfo
{
    public List<ProfileInfo> Profiles;

    public SaveInfo()
    {
        Profiles = new List<ProfileInfo>();
    }
}
