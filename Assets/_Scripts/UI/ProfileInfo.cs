﻿using System;

[Serializable]
public class ProfileInfo
{
    public string PlayerName;
    public int PlayerVictories;
    public int PlayerLosses;
}
