﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ProfileManager : MonoBehaviour 
{
    public RectTransform GridTransform;
    public ProfileEntry ProfileEntryPrefab;
    public int ProfileEntryCount;
    public InputField Name;
    public InputField Victories;
    public InputField Losses;
    public List<ProfileEntry> ProfileEntryList;
    
    private static ProfileManager _instance;
	
    public static ProfileManager Instance
    {
        get { return _instance ?? (_instance = FindObjectOfType<ProfileManager>()); }
    }

	void Start () 
    {           
        RefreshEntries();
    }

    ProfileEntry NewProfileEntry()
    {
        var profileEntry = Instantiate(ProfileEntryPrefab, GridTransform);
        ProfileEntryList.Add(profileEntry);
        profileEntry.Set();
        return profileEntry;
    }
    
    public void AddScores(Dictionary<string, bool> scores)
    {
        foreach (var profile in SaveManager.Instance.Save.Profiles)
        {
            if (!scores.ContainsKey(profile.PlayerName))
                continue;
            
            if (scores[profile.PlayerName])
                profile.PlayerVictories += 1;

            else
                profile.PlayerLosses += 1;
            
            scores.Remove(profile.PlayerName);
        }

        foreach (var newScore in scores)
        {
            var victory = 0;
            var loss = 1;

            if (newScore.Value)
            {
                victory = 1;
                loss = 0;
            }
            
            Create(newScore.Key, victory, loss);
        }
    }

    public void Create(string name, int victories, int losses)
    {
        var profile = new ProfileInfo
        {
            PlayerName = name, PlayerVictories = victories, PlayerLosses = losses
        };

        NewProfileEntry();

        SaveManager.Instance.Save.Profiles.Add(profile);
        SaveManager.Instance.SaveGame();
        
        RefreshEntries();
    }


    public void RefreshEntries()
    {
        foreach (var entry in ProfileEntryList)
        {
            Destroy(entry.gameObject);
        }

        var infoList = SaveManager.Instance.Save.Profiles;
        if (infoList == null)
            return;

        var orderedList = infoList.OrderByDescending(x => x.PlayerVictories - x.PlayerLosses).ToList();

        ProfileEntryList = new List<ProfileEntry>();
        
        for (var i = 1; i <= orderedList.Count; i++)
        {
            string suffix;
            switch (i)
            {
                case 1:
                    suffix = "st";
                    break;
                case 2:
                    suffix = "nd";
                    break;
                case 3:
                    suffix = "rd";
                    break;
                default:
                    suffix = "th";
                    break;
            }

            var entry = NewProfileEntry();
            entry.playerPlacing.text = i + suffix;
            entry.Set(orderedList[i - 1]);
        }
    }

    public void DeleteAndRefresh()
    {
        SaveManager.Instance.DeleteSaveGame();
        RefreshEntries();
    }
    
    private void OnDestroy()
    {
        if (_instance == this)
        {
            _instance = null;
        }
    }
}
