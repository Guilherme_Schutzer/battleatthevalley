﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ProfileEntry : MonoBehaviour
{

    public Text playerNameText;
    public Text playerVictoriesText;
    public Text playerLossesText;
    public Text playerPlacing;

    private GameObject popUp;

    public void Set(ProfileInfo profileInfo = null)
    {
        //playerProfile.SetActive(profileInfo != null);

        if (profileInfo != null)
        {
            playerNameText.text = profileInfo.PlayerName;
            playerVictoriesText.text = profileInfo.PlayerVictories.ToString();
            playerLossesText.text = profileInfo.PlayerLosses.ToString();
        }
    }
    
    public void Remove()
    {
        Debug.Log("Remove");
    }
}