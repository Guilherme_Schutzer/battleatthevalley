﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class SimpleNetworkHUD : MonoBehaviour
{

	public InputField InputAddress;
	public Button HostButton;
	public Button ClientButton;
	public InputField InputName;
	public InputField InputScore;
	
	private NetworkManager manager;
	private bool isNameValid;
	private bool isAddressValid = true;
	
	
	void Awake()
	{
		manager = GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManager>();
	}

	public void SetInteractableName(bool isValid)
	{
		isNameValid = isValid;
		SetInteractable();
	}

	public void SetInteractableAddress(bool isValid)
	{
		isAddressValid = isValid;
		SetInteractable();
	}

	private void SetInteractable()
	{
		HostButton.interactable = isNameValid;
		ClientButton.interactable = isNameValid && isAddressValid;
	}

	public void StartHost()
	{
		manager.networkAddress = InputAddress.text;
		manager.StartHost();
	}

	public void StartClient()
	{
		manager.networkAddress = InputAddress.text;
		manager.StartClient();
	}

	public void EnableNetworkHUD()
	{
		var networkManager = GameObject.FindGameObjectWithTag("NetworkManager");
		networkManager.GetComponent<NetworkManagerHUD>().enabled = true;
	}
	
}
