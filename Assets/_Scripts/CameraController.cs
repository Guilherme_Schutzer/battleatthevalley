﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class CameraController : MonoBehaviour
{
	private GameObject cameraTarget;
	public Vector3 Offset;
    public float Angle;
    public float turnSpeed = 4.0f;
	public bool clientRunning;

	private Vector3 originalPosition;

	private GameObject _target;
	public GameObject Target
	{
		get { return _target;}
		set {
			if (_target == value) return;
			_target = value;
			OnTargetChanged();
		}
	}

	void Start()
	{
		originalPosition = gameObject.transform.position;
	}

	void OnTargetChanged()
	{
		if (Target == null)
		{
			clientRunning = false;
            return;
		}

		clientRunning = true;
	}


    void LateUpdate()
    {
        var mouse = Input.GetAxis("Mouse X");
        Offset = Quaternion.AngleAxis(mouse * turnSpeed, Vector3.up) * Offset;
	    if (clientRunning)		    
	    {
		    transform.position = Target.transform.position + Offset;
		    transform.LookAt(Target.transform.position);
	    }
	    else
	    {
		    gameObject.transform.position = originalPosition;
	    }
	     
    }
}