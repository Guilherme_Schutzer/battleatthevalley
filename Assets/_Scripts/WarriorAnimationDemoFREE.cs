﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RPGCharacterAnims;
using UnityEngine.AI;
using UnityEngine.Networking;
using _Scripts;
using Random = UnityEngine.Random;

public class WarriorAnimationDemoFREE : Damageable
{
    public GameObject Hammer;

    Vector3 targetDirection;
    private float rotationSpeed = 120f;
    private BoxCollider hammerCollider;
    private NavMeshAgent nav;
    private GameObject ClosestPlayer;
    private List<GameObject> Players;

    void Start()
    {
        base.Start();
        hammerCollider = Hammer.transform.GetChild(0).GetComponent<BoxCollider>();
        hammerCollider.enabled = false;
        nav = gameObject.GetComponent<NavMeshAgent>();
        Players = GameObject.FindGameObjectWithTag("Info").GetComponent<GameInfo>().Players;
    }

    private float FindClosestPlayer()
    {
        ClosestPlayer = null;
        var closestDistance = float.MaxValue;
        foreach (var player in Players)
        {
            if (player == null)
                continue;

            var playerDistance = Vector3.Distance(player.transform.position, transform.position);
            if (playerDistance < closestDistance && !player.GetComponent<RPGCharacterControllerFREE>().IsDead)
            {
                closestDistance = playerDistance;
                ClosestPlayer = player;
            }
        }
        return closestDistance;
    }

    private void UpdateNav()
    {
        var closestDistance = FindClosestPlayer();
        if (ClosestPlayer != null && closestDistance < 50)
        {
            nav.enabled = true;
            nav.SetDestination(ClosestPlayer.transform.position);
        }
        else
        {
            nav.enabled = false;
        }
    }

    void RotateTowardsTarget(Vector3 targetPosition)
    {
        Quaternion targetRotation =
            Quaternion.LookRotation(targetPosition - new Vector3(transform.position.x, 0, transform.position.z));
        transform.eulerAngles = Vector3.up * Mathf.MoveTowardsAngle(transform.eulerAngles.y,
                                    targetRotation.eulerAngles.y, (rotationSpeed * Time.deltaTime) * rotationSpeed);
    }

    private void OnAnimatorMove()
    {
        transform.position = nav.nextPosition;
    }

    void Update()
    {
        
        if (nav.velocity != Vector3.zero || !canMoveAction)
        {
            //set that character is moving
            animator.SetBool("Moving", true);
        }
        else
        {
            //character is not moving
            animator.SetBool("Moving", false);
        }
        
        if (!canMoveAction)
        {
            nav.enabled = false;
            return;
        }

        UpdateNav();

        if (ClosestPlayer != null && Vector3.Distance(ClosestPlayer.transform.position, transform.position) < 3)
        {
            Attack();
        }
    }

    void Attack()
    {
        RotateTowardsTarget(ClosestPlayer.transform.position);
        TimedLock(0.4f);
        animator.SetTrigger("Attack1Trigger");
        networkAnimator.SetTrigger("Attack1Trigger");
    }

    public override void TimedLock(float seconds)
    {
        StartCoroutine(_Lock(seconds));
    }

    public IEnumerator _Lock(float seconds)
    {
        canMoveAction = false;
        yield return new WaitForSeconds(seconds);
        canMoveAction = true;
    }

    void EnableHammer()
    {
        hammerCollider.enabled = true;
    }

    void DisableHammer()
    {
        hammerCollider.enabled = false;
    }

    public override void Death()
    {
        SpawnPowerUp();
    }
    
    public override void Blink(float totalSeconds)
    {
        const float fixedInterval = 0.1f;
        var numBlink = totalSeconds / fixedInterval / 2;
        StartCoroutine(_Blink((int)numBlink, fixedInterval));
    }
    
    public void SpawnPowerUp()
    {
        if (!isServer) 
            return;
        
        int prefabIndex;
        var r = Random.value;
        
        if (ClosestPlayer.GetComponent<Damageable>().CurrentHealth >= 80)
        {
            prefabIndex = r <= 0.5f ? 0 : 1;
        }
        else
        {
            prefabIndex = r <= 0.25f ? 0 : r <= 0.50 ? 1 : 2;
        }
        RpcSpawnPowerUp(prefabIndex);
    }

    [ClientRpc]
    public void RpcSpawnPowerUp(int prefabIndex)
    {
        var powerUpPrefabs = GameObject.FindGameObjectWithTag("Info").GetComponent<GameInfo>().PowerUpPrefabs;
        var position = gameObject.transform.position + new Vector3(0, 0.7f, 0);
        
        var powerUp = Instantiate(
            powerUpPrefabs[prefabIndex],
            position,
            gameObject.transform.rotation);
        
        Destroy(powerUp, 30.0f);
        Destroy(gameObject);
    }
}