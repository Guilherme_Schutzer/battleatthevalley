﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// Spawn point workaround to deal with terrain not working with
/// mesh collider trees. Spawns 3 types of tree depending on height.
/// </summary>
public class TreeSpawnPoint : NetworkBehaviour {

    public GameObject[] Prefabs;

    private float[] prefabChances;
    private Vector3 angles;
    private float size;
    private int prefabIndex;

	// Use this for initialization
	public override void OnStartServer () {

        prefabChances = new float[Prefabs.Length];
        angles = new Vector3(0, Random.value * 360, 0);
        size = Random.value + 2.5f;

        var height = gameObject.transform.position.y;
        if (height < 5)
        {
            prefabChances = new[] { 0.6f, 0.35f, 0.05f };
        }
        else if (height < 30)
        {
            prefabChances = new[] { 0.3f, 0.45f, 0.25f };
        }
        else if (height < 50)
        {
            prefabChances = new[] { 0.05f, 0.7f, 0.25f };
        }
        else
        {
            prefabChances = new[] { 0.0f, 0.05f, 0.95f };
        }

        var randomValue = Random.value;
        if (randomValue <= prefabChances[0])
        {
            prefabIndex = 0;
        }
        else if (randomValue <= prefabChances[0] + prefabChances[1])
        {
            prefabIndex = 1;
        }
        else
        {
            prefabIndex = 2;
        }

        var tree = Instantiate(
            Prefabs[prefabIndex],
            gameObject.transform.position,
            Quaternion.Euler(angles));

        tree.transform.localScale = new Vector3(size, size, size);
        tree.transform.SetParent(gameObject.transform);

        NetworkServer.Spawn(tree);
    }
}
