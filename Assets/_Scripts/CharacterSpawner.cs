﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CharacterSpawner : NetworkBehaviour
{
	public GameObject Prefab;
	
	public void SpawnCharacter()
	{
		var character = Instantiate(
			Prefab,
			gameObject.transform.position,
			gameObject.transform.rotation);

		character.transform.SetParent(gameObject.transform);
		NetworkServer.Spawn(character);
	}
}
