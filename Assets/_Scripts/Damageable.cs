﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public abstract class Damageable : NetworkBehaviour {

    public float MaxHealth;

    [SyncVar(hook = "OnChangeHealth")] 
    public float CurrentHealth;
    
    public float knockbackMultiplier;
    public Renderer[] Renderers;
    public SkinnedMeshRenderer[] SkinnedMeshRenderers;

    public Rigidbody rigidBody;
    public Animator animator;
    public NetworkAnimator networkAnimator;
    public RectTransform HealthBar;
    
    [HideInInspector] public bool isVulnerable = true;
    [HideInInspector] public bool isKnockback = false;
    [HideInInspector] public bool canMoveAction = true;

    private float startingBarLength = 100;
    
    public void Start()
    {
        rigidBody = gameObject.GetComponent<Rigidbody>();
        animator = gameObject.GetComponent<Animator>();
        networkAnimator = gameObject.GetComponent<NetworkAnimator>();
        CurrentHealth = MaxHealth;
    }

    public virtual void GetHit(Vector3 direction, float damage)
    {
        if (!isVulnerable)
        {
            return;
        }

        isVulnerable = false;
        TakeDamage(damage);
        if (rigidBody != null)
        {   
            StartCoroutine(_Knockback(direction, 8 * ((int)damage / 10), 4));
        }
    }

    public virtual void TakeDamage(float damage)
    {
        CurrentHealth -= damage;
        if (CurrentHealth <= 0)
        {
            Death();
            isVulnerable = false;
        }
        else
        {
            TimedLock(0.4f);            
        }
        Blink(1f);
    }

    void OnChangeHealth(float health)
    {
        if (HealthBar == null)
            return;
        
        HealthBar.sizeDelta = new Vector2(health * startingBarLength / MaxHealth, HealthBar.sizeDelta.y );
    }

    public abstract void Blink(float seconds);
    
    
    public IEnumerator _Blink(int n, float intervalSeconds)
    {
        for (var i = 0; i < n * 2; i++)
        {
            foreach (var renderer in Renderers)
            {
                renderer.enabled = !renderer.enabled;
            }
            foreach (var renderer in SkinnedMeshRenderers)
            {
                renderer.enabled = !renderer.enabled;
            }
            yield return new WaitForSeconds(intervalSeconds);
        }

        foreach (var renderer in Renderers)
        {
            renderer.enabled = true;
        }
        foreach (var renderer in SkinnedMeshRenderers)
        {
            renderer.enabled = true;
        }

        isVulnerable = true;
    }

    private IEnumerator _Knockback(Vector3 knockDirection, int knockBackAmount, int variableAmount)
    {
        isKnockback = true;
        knockDirection.y = 0;
        StartCoroutine(_KnockbackForce(knockDirection, knockBackAmount, variableAmount));
        yield return new WaitForSeconds(.1f);
        isKnockback = false;
    }

    IEnumerator _KnockbackForce(Vector3 knockDirection, int knockbackAmount, int variableAmount)
    {
        while (isKnockback)
        {
            rigidBody.AddForce(
                knockDirection * ((knockbackAmount + Random.Range(-variableAmount, variableAmount)) *
                                  (knockbackMultiplier * 10)), ForceMode.Impulse);
            yield return null;
        }
    }

    public abstract void TimedLock(float seconds);

    public abstract void Death();
}
