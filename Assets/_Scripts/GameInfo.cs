﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RPGCharacterAnims;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class GameInfo : NetworkBehaviour
{
	public List<GameObject> Players;
	public List<GameObject> PowerUpPrefabs;

	private Dictionary<string, bool> scores;
	private NetworkManager networkManager;

	public int NumberOfPlayers;
	public string WinnerName;

	void Awake()
	{
		networkManager = GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManager>();
	}

	public override void OnStartServer()
	{
		scores = new Dictionary<string, bool>();
	}

	public void RegisterDeath(string name)
	{
		scores.Add(name, false);
		NumberOfPlayers -= 1;
		if (NumberOfPlayers <= 1)
		{
			FinishGame();
		}
	}
	
	void FinishGame()
	{
		GameObject winner = null;

		foreach (var player in Players)
		{
			Debug.Log(player.GetComponent<RPGCharacterControllerFREE>().PlayerName);
			if (!scores.ContainsKey(player.GetComponent<RPGCharacterControllerFREE>().PlayerName))
			{
				winner = player;
			}
		}

		if (winner != null)
		{
			WinnerName = winner.GetComponent<RPGCharacterControllerFREE>().PlayerName;
			scores.Add(WinnerName, true);
		}
		else
		{
			Debug.Log("Nobody wins.");
		}

		StartCoroutine(_WaitAndFinish());
	}

	IEnumerator _WaitAndFinish()
	{
		ProfileManager.Instance.AddScores(scores);
		SaveManager.Instance.SaveGame();
		ProfileManager.Instance.RefreshEntries();
		var saveJson = JsonUtility.ToJson(SaveManager.Instance.Save);
		
		foreach (var player in Players)
		{
			player.GetComponent<RPGCharacterControllerFREE>()
				.RpcFinishGame(saveJson);
		}
		
		yield return new WaitForSeconds(2.0f);

		networkManager.StopHost();
	}
}
