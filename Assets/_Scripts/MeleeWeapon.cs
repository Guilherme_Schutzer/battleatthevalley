﻿using UnityEngine;

namespace _Scripts
{
	public class MeleeWeapon : MonoBehaviour
	{
		public float MeleePower;
	
		private void OnTriggerEnter(Collider other)
		{
			if (other.gameObject.CompareTag("Player"))
			{
				var damageable = other.gameObject.GetComponent<Damageable>();
				if (damageable != null)
				{
					damageable.GetHit(other.gameObject.transform.position - gameObject.transform.position, MeleePower);
				}
			
			}
		}
	}
}
